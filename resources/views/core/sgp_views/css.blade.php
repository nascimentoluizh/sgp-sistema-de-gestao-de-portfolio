<link rel="shortcut icon" type="image/png" href="/sgp_files/assets/images/icon/favicon.ico">
<link rel="stylesheet" href="/sgp_files/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/sgp_files/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="/sgp_files/assets/css/themify-icons.css">
<link rel="stylesheet" href="/sgp_files/assets/css/metisMenu.css">
<link rel="stylesheet" href="/sgp_files/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/sgp_files/assets/css/slicknav.min.css">
<!-- amchart css -->
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<!-- others css -->
<link rel="stylesheet" href="/sgp_files/assets/css/typography.css">
<link rel="stylesheet" href="/sgp_files/assets/css/default-css.css">
<link rel="stylesheet" href="/sgp_files/assets/css/styles.css">
<link rel="stylesheet" href="/sgp_files/assets/css/responsive.css">
<!-- modernizr css -->
<script src="/sgp_files/assets/js/vendor/modernizr-2.8.3.min.js"></script>
{{-- Fonte Awesome --}}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">