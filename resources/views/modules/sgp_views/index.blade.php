@extends('core.sgp_views.app')

@section('content')
<div class="container">
    <div class="sgp-index">
        <h1>SGP - Sistema de Gerenciamento de Portifolio</h1>

        <div class="sgp-cards">
            <div class="row">
                <div class="col-md-6">
                    <div class="sgp-card-primary">
                        <p class="sgp-card-title"><i class="fas fa-clipboard fa-2x"></i> Postagens</p>

                        <p class="sgp-card-content">
                            Total Cadastrado: 20 <br>
                            Categoria Dominante: Agricultura
                        </p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="sgp-title-green"><i class="fas fa-book-open"></i> Lista de Postagens</div>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Categoria</th>
                                <th scope="col">Titulo</th>
                                <th scope="col">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Agricultura</th>
                                <td>Mark</td>
                                <td>Otto</td>
                            </tr>
                            <tr>
                            <th scope="row">Agricultura</th>
                                <td>Um marco...</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection